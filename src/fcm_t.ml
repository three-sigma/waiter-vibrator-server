(* Auto-generated from "fcm.atd" *)
              [@@@ocaml.warning "-27-32-35-39"]

type data = { token: string }

type message = { topic: string; data: data }
