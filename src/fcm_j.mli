(* Auto-generated from "fcm.atd" *)
[@@@ocaml.warning "-27-32-35-39"]

type data = Fcm_t.data = { token: string }

type message = Fcm_t.message = { topic: string; data: data }

val write_data :
  Bi_outbuf.t -> data -> unit
  (** Output a JSON value of type {!data}. *)

val string_of_data :
  ?len:int -> data -> string
  (** Serialize a value of type {!data}
      into a JSON string.
      @param len specifies the initial length
                 of the buffer used internally.
                 Default: 1024. *)

val read_data :
  Yojson.Safe.lexer_state -> Lexing.lexbuf -> data
  (** Input JSON data of type {!data}. *)

val data_of_string :
  string -> data
  (** Deserialize JSON data of type {!data}. *)

val write_message :
  Bi_outbuf.t -> message -> unit
  (** Output a JSON value of type {!message}. *)

val string_of_message :
  ?len:int -> message -> string
  (** Serialize a value of type {!message}
      into a JSON string.
      @param len specifies the initial length
                 of the buffer used internally.
                 Default: 1024. *)

val read_message :
  Yojson.Safe.lexer_state -> Lexing.lexbuf -> message
  (** Input JSON data of type {!message}. *)

val message_of_string :
  string -> message
  (** Deserialize JSON data of type {!message}. *)

