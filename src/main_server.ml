open Lwt
open Cohttp
open Cohttp_lwt_unix

let server_key = ref ""
let port = ref 8080

let usage = "usage: " ^ Sys.argv.(0) ^ " [-k string] [-p int]"

let speclist = [
    ("-k", Arg.Set_string server_key, "server key");
    ("-p", Arg.Set_int port, "port, default 8080");
]

let () =
  Arg.parse
    speclist
    (fun x -> raise (Arg.Bad ("Bad argument : " ^ x)))
    usage
    
module Notifier = Fcm_notifier.Make(struct let server_key = !server_key end)

let parse_uri req body =
  Notifier.post_message () >>= (fun () -> Server.respond_string ~status:`OK ~body:"" ())

let server = 
  let callback _ req body =
    parse_uri req body 
  in
  Server.create ~mode:(`TCP (`Port !port)) (Server.make ~callback ());;

Lwt.async_exception_hook := (function
  | Unix.Unix_error (error, func, arg) ->
    Logs.warn (fun m ->
      m  "Client connection error %s: %s(%S)"
        (Unix.error_message error) func arg
    )
  | exn -> Logs.err (fun m -> m "Unhandled exception: %a" Fmt.exn exn)
)

let () = ignore (Lwt_main.run server)